# Python Hooks

This repository allows us to share/centralize our Python [pre-commit](https://pre-commit.com/) configuration,
saving us from copy-pasting hell between our projects.

[[_TOC_]]

## Quickstart

In the top level directory of your Python project, dump the following content to a file
named `.pre-commit-config.yaml`:

```yaml
repos:
- repo: https://gitlab.m2m.axione.fr/axione/elbaf/python-hooks
  rev: latest
  hooks:
  - id: python-hooks
    verbose: true
```

Then run:

```shell
pre-commit run -a
```

**Note:** For this to work, you must have `pre-commit` [installed](https://pre-commit.com/#installation)
in your (virtual) environment.

## Problem

Within our Python, we use several quality/linting tools like `mypy`, `black` and `pyupgrade` for instance, to ensure that
we write our code according to the community best practices. To run these checkers we rely on the tool `pre-commit`.
This way, each time we commit our code, pre-commit automatically runs our checks for us.
`pre-commit` relies on a configuration to know what checks to make.
Here is a simple example of such a configuration, that must be stored in a `.pre-commit-config.yaml` file at the root
of a project:

The problem we have is that we want all our projects to follow the same rules. As a consequence we need to copy the
same configuration over and over into each and every projects of our own.
This is a cumbersome and error prone process which scales difficultly. Each time we want to introduce a new linter
or change a parameter of an existing one, we need to run across all our projects and make the same modification.
This is too heavy of a process, we need something better.


## Solution


### Expliquer le truc des hooks tous contenus dans un seul, expliquer ce que fais l'implémentation

## Drawbacks

This solution has two drawbacks.
First, the centralisation of the configuration could become a problem over time. The more projects we have, the more
likely it will be that we'll need some customisations specific to a project and this will be difficult to achieve.
The second is related to how the pre-commit report is displayed.


## Changing the configuration

For this you will have to read both the documentation of pre-commit as well as the tool that you want to integrate
and then make changes to the file `python_hooks/config.yaml`






Le support pour la centralisation a ete officiellement refusée icic

MAis l'auteru machin de la lib a propose une solution ici sur stackoverflo


https://stackoverflow.com/a/59055569




Utilisation

Mettre ça dans votre preconfig.yaml




Avantages
On centralise la conf


Incovenients

Souplesse
Si nous avons besoin de faire des reglages fins et que notre nombre de repos augmente cela pourrait poser problème

Affichage
Etant donné qu'on run pc dans pc, notre ensemble de hooks et considéré comme un seul hook et on a donc l'affichage que a la fin




Tag
tag avec une version et avec latest


parler du pdm lock et du fix de versions en ==
mettre le code dans product
run le repo sur lui même

