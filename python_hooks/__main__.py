#!/usr/bin/env python
import os
import sys


def main():
    this_directory_path = os.path.dirname(os.path.realpath(__file__))
    config_path = os.path.join(this_directory_path, 'pre-commit-config.yaml')
    input_files = sys.argv[1:]
    command = ['pre-commit', 'run', '--config', config_path, '--files', *input_files]
    os.execvp(command[0], command)


if __name__ == '__main__':
    exit(main())
